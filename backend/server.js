const express = require("express");
const mysql = require("mysql2");
const routerEmp = require("./routes/emp");
const cors = require("cors");

const app = express();
app.use(cors("*"));
app.use(express.json());
app.use("/emp", routerEmp);

app.listen(4000, "0.0.0.0", () => {
  console.log(`server started on port 4000`);
});
