const express = require("express");
const db = require("../db");
const utils = require("../utils");
const router = express.Router();

router.post("/add", (request, response) => {
  console.log("IN POST ADD");
  const { emp_name, salary, age } = request.body;
  const query = `
          insert into emptb
          (emp_name, salary, age)
          values
            ('${emp_name}', '${salary}', '${age}')
        `;

  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

router.delete("/:empid", (request, response) => {
  console.log("IN DEWLETE DELEYE");
  const { empid } = request.params;

  const query = `
      delete from emptb
      where
      empid = ${empid}
    `;

  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

router.put("/update/:empid", (request, response) => {
  console.log("IN PUT UPDATE");
  const { empid } = request.params;
  const { salary, age } = request.body;

  const query = `
      update emptb
      set
      salary = '${salary}',
      age = '${age}'
      where
      empid = ${empid}
    `;
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

router.get("/get", (request, response) => {
  console.log("IN GET GET");
  const query = `
        select
          empid,
          emp_name,
          salary,
          age
        from emptb
      `;
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

module.exports = router;
